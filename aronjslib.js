// Load text from TextArea                                                
// Highlight the text and add to div ID.RENDERID                          
//
//   Use symbol link in your local java/html code
//   $b/jslib/aronlib.js
// 
//   <head>
//   <script src="aronlib.js"></script>
//  </head>

function test(){
    alert('/Users/cat/myfile/bitbucket/jslib/aronlib.js test() 1');
}

var delay = function(value){
    return function(){
        doneTyping(value);
    };
}


// split string with newline '\n'
// trim all string, filter out zero length string
// return an array
function stringToArray(strs){
    var arr = strs.split('\n');
    var newArr = arr.filter(x => x.trim().length > 0)  
    return newArr;
}


function takeList(n, list){
    return list.slice(0, n);
}

function dropList(n, list){
    return list.slice(n);
}
function headList(list){
    return list.slice(1, n);
}

function lastList(list){
    return list[list.length - 1]; 
}

function tailList(list){
    return dropList(1, list);
}

function compareArray(arr1, arr2){
    if(arr1.length == arr2.length){
        for(var i=0; i<arr1.length; i++){
            if(arr1[i] != arr2[i])
                return false;
        }
    }
    return true;
}

function prefixStr(str){
    var list = [];
    for(var i=0; i<str.length; i++){
        list.push(str.substr(0, i+1)); 
    }
    return list;
}



let g_numToLineMap = new Map();

const CONS = {
    NL : '\n',  // newline
    HTMLNL : '<br>', // html newline
    UNDEF : undefined,
    EMPTY : '' 
}

const ID={
    RENDER : 'render',
    TEXTAREA : 'txtAreaId'
}

// MacOS
const KEY = {
    DELETE : 8,
    ENTER  : 13,
    LEFT   : 37,
    UP     : 38,
    RIGHT  : 39,
    DOWN   : 40,
    TAB   : 9 
}

function chop(n, str){
    var fst = take(n, str);
    var snd = drop(n, str);
    return {
        fst : fst,
        snd : snd
    };
}

function take(n, str){
    return str.substr(0, n);
}

function len(s){
    return s.length;
}

function drop(n, str){
    return str.substr(n, str.length);
}

function prefixMap2(list){
    var map = new Map();
    for(var i=0; i<list.length; i++){
        for(var j=0; j<list[i].length; j++){
            var ps = chop(j + 1, list[i]);
            if(ps.fst.length > 0 && ps.snd.length > 0){
                var v = map.get(ps.fst);
                if(v == undefined){
                    v = [];
                }
                v.push(ps.snd);
                map.set(ps.fst, v);
            }
        }
    }
    return map;
}

function prefixMap(list){
    var map = new Map();
    for(var i=0; i<list.length; i++){
        var preList = prefixStr(list[i]);
        for(var j=0; j<preList.length; j++){
            var v = map.get(preList[j]);
            if(v == undefined){
                v = [];
            }
            v.push(list[i]);
            map.set(preList[j], v);
        }
    }
    return map;
}


class LineTxtCSS{
    text = '';
    cssText = '';
    constructor(text, cssText){
        this.text = text;
        this.cssText = cssText;
    }
}

function color(s) {
    return "<span style='color:green;'>" + s + "</span>";
}

function colorx(s, col) {
    return "<span style='color:" + col + "';>" + s + "</span>";
}


function logKey(e) {
  // alert(e.code);
}


function isPrintableKey(keycode){
    const valid = 
          (keycode > 47 && keycode < 58)   || // number keys
        keycode == 32                    || // spacebar & return key(s) (if you want to allow carriage returns)
	(keycode > 64 && keycode < 91)   || // letter keys
        (keycode > 95 && keycode < 112)  || // numpad keys
        (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
        (keycode > 218 && keycode < 223); 
    return valid;
}

// return the begin index of string
function begWord(str, start){
    var beg = 0;
    for(var i=start; i>=0; i--){
        if(str[i] == ' '){
            beg = i;
            break;
        }
    }
    return beg;
}

function buildPrefixMapFromId(id){
    var els = getElem(id);
    var strList = els.innerHTML.split('\n');
    var fList = strList.filter(function(x){ return x.length > 0 })
    var map = prefixMap2(fList);
    return map;
}

  function autoCompleteFun(event){
        if(event.keyCode == KEY.LEFT){                       
            var html = getSelectionHtml();
            var txtArea = getElem(ID.TEXTAREA);            	  
            var autoCompleteDiv = getElem('autoCompleteDiv');
	    txtArea.setRangeText(html);                	  
          
	    autoCompleteDiv.innerHTML = CONS.EMPTY;        	  
            hideById('autoCompleteDiv');                   
                                                             
            var renderId = getElem(ID.RENDER);             
            g_numToLineMap = buildLineMap(txtArea.value);  
            renderId.innerHTML = parse(txtArea.value);
	    
            var updatedTxtArea = getElem(ID.TEXTAREA);
            setTxtAreaCursorXYOffset(updatedTxtArea, html.length);
            var start = updatedTxtArea.selectionStart;
	    setCaretToPos(updatedTxtArea, start + html.length, start + html.length);
            txtArea.focus();                           	  
        } else if((event.keyCode == KEY.UP || event.keyCode == KEY.DOWN)){
            var autoCompleteDiv = getElem('autoCompleteDiv');
	}
  }                                                              
function setSelectionRange(input, selectionStart, selectionEnd) {
  if (input.setSelectionRange) {
    input.focus();
    input.setSelectionRange(selectionStart, selectionEnd);
  }
  else if (input.createTextRange) {
    var range = input.createTextRange();
    range.collapse(true);
    range.moveEnd('character', selectionEnd);
    range.moveStart('character', selectionStart);
    range.select();
  }
}

function setCaretToPos (input, pos) {
   setSelectionRange(input, pos, pos);
}
  


function setTxtAreaCursorXYOffset(txtArea, offset){
    var xy = getCursorXY(txtArea, txtArea.selectionStart + offset);        
    var mycursor = getElem('fakeCursorId');                     
    const LEFT = -8;                                              
    const TOP = -7;                                             
    mycursor.style.left = xy.x + LEFT + xy.spanX + 'px';          
    mycursor.style.top  = xy.y + TOP + xy.spanY + 'px';         
                                                                  
    var autoCompleteDiv = getElem('autoCompleteDiv');           
    autoCompleteDiv.style.left = xy.x + LEFT + xy.spanX + offset + 'px'; 
    autoCompleteDiv.style.top  = xy.y + TOP + xy.spanY + 'px';    
}                                                               
  

  
function setTxtAreaCursorXY(txtArea){
    var xy = getCursorXY(txtArea, txtArea.selectionStart);
    var mycursor = getElem('fakeCursorId');
    const LEFT = -8;
    const TOP = -7;

    mycursor.style.left = xy.x + LEFT + xy.spanX + 'px';
    mycursor.style.top  = xy.y + TOP + xy.spanY + 'px';

    var autoCompleteDiv = getElem('autoCompleteDiv');
    autoCompleteDiv.style.left = xy.x + LEFT + xy.spanX + 'px';
    autoCompleteDiv.style.top  = xy.y + TOP + xy.spanY + 'px';
}

// element
function getCurrRow(txtArea){
    var str = txtArea.value;
    var arr = str.split('\n');

    var start = txtArea.selectionStart;
    var end = txtArea.selectionEnd;

    var sum = 0;
    var nrow = 0;
    var offset = -1;
    for(var i=0; i<arr.length; i++){
        if((sum + arr[i].length) < end){
           sum += (arr[i].length + 1);
           nrow++;
        }else if((sum + arr[i].length) >= end){
           offset = end - sum;
           break;
        } 
    }

    return {
        nrow : nrow,
        offset : offset,
        lines : arr
    };
}

  function donothing(event){
      if(event.keyCode == KEY.DOWN){
	  if(getElem('autoCompleteDiv').style.display == 'block'){
	      getElem('autoCompleteDiv').focus();
	      return autoCompleteFun(event);
	  }
	  return false;
      }
  }

function buildLineMap(str){
    var map = new Map();
    var list = str.split(CONS.NL);
    for(var i=0; i<list.length; i++){
        console.log('s=' + list[i]);
        var cssLine = parse(list[i]);
        var lineTxtCSS = new LineTxtCSS(list[i], cssLine);
        map.set(i, lineTxtCSS); 
    }
    return map;
}


function parseMap(map, nrow){
    var lineObj = map.get(nrow);
    var cssLine = parse(lineObj.cssText);
    // Update the global map, {"", new line, new css line}
    var lineTxtCSS = new LineTxtCSS(map.lines[nrow], cssLine);
    map.set(nrow, lineTxtCSS); 
    var retStr = '';
    for(var i=0; i<map.size; i++){
        if(i != nrow){
            retStr += map.get(i).cssText + CONS.HTMLNL; 
        }else{
            retStr += cssLine + CONS.HTMLNL; 
        }
    }
    return retStr;
}




function parse(str){
    var retStr = '';
    for(var i=0; i<str.length; i++) {
        if(str[i] == '{' || str[i] == '}') {
            retStr += colorx(str[i], 'red');
        } else if(str[i] == '[' || str[i] == ']') {
            retStr += colorx(str[i], 'pink');
        } else if(str[i] == '(' || str[i] == ')') {
            retStr += colorx(str[i], 'green');
        } else if(str[i] == CONS.NL){
            retStr += '<br>'
        } else if(str[i] == '<'){
            retStr +='&lt;'
        } else if(str[i] == '>'){
            retStr += '&gt;'
        }else if(str[i] == '"'){
            var tokenStr = '"';
            var j = i+1;
            for(j=i+1; j<str.length; j++){
                if(str[j] != '"'){
                    tokenStr += str[j]; 
                }else{
                    tokenStr += str[j];
                    retStr += colorx(str.substr(i, (j-i)+1), 'red');
                    i = j;
                    break;
                }
            }
        }else{
            retStr += str[i]; 
        }
    }
    return retStr;
}

function parseStr(str, inx){
    let ret = '';
    let retInx = -1;
    if(str[inx] == '"'){
	for(var i=inx + 1; i<str.length; i++){
	    if(str[i] != '"'){
		retInx = i;
		break;
	    }else{
		ret += str[i]; 
	    }
	}
    }
    if(retInx > -1){
	return {
	    str : ret,
	    i : retInx
	}
    }else{
	return {
	    str : undefined,
	    i : retInx
	}
    }
}
  function parse2(str){
    var listPair = [];
    var retStr = '';                                                     
    for(var i=0; i<str.length; i++) {                                      
        if(str[i] == '{' || str[i] == '}') {
            listPair.push([i, i]);
            retStr += colorx(str[i], 'red');                             
        } else if(str[i] == '[' || str[i] == ']') {
            listPair.push([i, i]);
            retStr += colorx(str[i], 'pink');                            
        } else if(str[i] == '(' || str[i] == ')') {
            listPair.push([i, i]);
            retStr += colorx(str[i], 'green');                           
        } else if(str[i] == CONS.NL){                                    
            retStr += '<br>'                                               
        } else if(str[i] == '<'){                                        
            retStr +='&lt;'                                              
        } else if(str[i] == '>'){                                          
            retStr += '&gt;'                                             
        }else if(str[i] == '"'){                                         
            var tokenStr = '"';                                          
            var j = i+1;                                                 
            for(j=i+1; j<str.length; j++){                                 
                if(str[j] != '"'){                                       
                    tokenStr += str[j];                                    
                }else{                                                   
                    tokenStr += str[j];                                  
                    retStr += colorx(str.substr(i, (j-i)+1), 'red');     
                    listPair.push([i, j]);
                    i = j;
		    // string: ["dogcat"]
                    break;                                                 
                }                                                        
            }                                                            
        }else{                                                           
            retStr += str[i];                                            
        }                                                                
    }                                                                      
    return listPair;                                                       
}                                                                        

  



/**
* Get line content from div element
* 
* Ref: https://stackoverflow.com/questions/46253901/javascript-get-html-of-current-line-in-contenteditable-div
* 
*/



function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (25+o.scrollHeight)+"px";
}



